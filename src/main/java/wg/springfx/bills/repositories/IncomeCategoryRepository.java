package wg.springfx.bills.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import wg.springfx.bills.models.IncomeCategory;

public interface IncomeCategoryRepository extends JpaRepository<IncomeCategory, String> {
}
