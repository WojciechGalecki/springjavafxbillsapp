package wg.springfx.bills.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import wg.springfx.bills.models.Income;

import java.util.List;

public interface IncomeRepository extends JpaRepository<Income, Long> {
}
