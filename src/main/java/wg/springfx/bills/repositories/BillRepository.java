package wg.springfx.bills.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import wg.springfx.bills.models.Bill;

public interface BillRepository extends JpaRepository<Bill, Long> {
}
