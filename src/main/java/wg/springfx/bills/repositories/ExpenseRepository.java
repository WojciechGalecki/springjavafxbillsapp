package wg.springfx.bills.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import wg.springfx.bills.models.Expense;

public interface ExpenseRepository extends JpaRepository<Expense, Long> {
}
