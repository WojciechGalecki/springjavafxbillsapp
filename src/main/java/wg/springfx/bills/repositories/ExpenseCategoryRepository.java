package wg.springfx.bills.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import wg.springfx.bills.models.ExpenseCategory;

public interface ExpenseCategoryRepository extends JpaRepository<ExpenseCategory, String> {
}
