package wg.springfx.bills.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Bill {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private LocalDate creationDate;
    private Double balance;

    @OneToMany(mappedBy = "bill")
    private List<Income> incomes;

    @OneToMany(mappedBy = "bill")
    private List<Expense> expenses;
}
