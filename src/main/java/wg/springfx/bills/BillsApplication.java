package wg.springfx.bills;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class BillsApplication extends Application {

    private ConfigurableApplicationContext ctx;
    private Parent root;

    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void init() throws Exception {
        ctx = SpringApplication.run(BillsApplication.class);
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/Main.fxml"));
        fxmlLoader.setControllerFactory(ctx::getBean);
        root = fxmlLoader.load();
        root.getStylesheets().add("/css/main.css");
    }

    @Override
    public void start(Stage stage) throws Exception {
        stage.setScene(new Scene(root));
        stage.setTitle("MyBills");
        stage.setResizable(false);
        stage.show();
    }

    @Override
    public void stop() throws Exception {
        ctx.close();
    }
}
