package wg.springfx.bills.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import wg.springfx.bills.services.CategoryService;
import wg.springfx.bills.services.IncomeService;

import java.time.LocalDate;

@Controller
public class MainController {

    private CategoryService categoryService;
    private IncomeService incomeService;

    @Autowired
    public MainController(CategoryService categoryService, IncomeService incomeService) {
        this.categoryService = categoryService;
        this.incomeService = incomeService;
    }

    @FXML
    private Label dateLabel;
    @FXML
    private ChoiceBox<String> incomeCategory;
    @FXML
    private TextField newIncomeCategory;
    @FXML
    private TextField incomeValue;

    public void initialize() {
        String date = LocalDate.now().toString();
        dateLabel.setText(date);

        showIncomeCategory();
    }

    public void addIncome() {
        incomeService.addIncome(incomeCategory, incomeValue);
    }

    private void showIncomeCategory() {
        incomeCategory.setItems(categoryService.getCategory());
        incomeCategory.setValue(categoryService.getCategory().get(0));
    }
}
