package wg.springfx.bills.services;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import wg.springfx.bills.repositories.IncomeCategoryRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class IncomeCategoryService implements CategoryService {

    private IncomeCategoryRepository incomeCategoryRepository;

    @Autowired
    public IncomeCategoryService(IncomeCategoryRepository incomeCategoryRepository) {
        this.incomeCategoryRepository = incomeCategoryRepository;
    }

    @Override
    public ObservableList<String> getCategory() {
        List<String> categories = new ArrayList<>();
        incomeCategoryRepository.findAll().iterator().forEachRemaining(income -> categories.add(income.getCategory()));

        return FXCollections.observableList(categories);
    }
}
