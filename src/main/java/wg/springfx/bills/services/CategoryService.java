package wg.springfx.bills.services;

import javafx.collections.ObservableList;

public interface CategoryService {

    ObservableList<String> getCategory();
}
