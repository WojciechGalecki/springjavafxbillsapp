package wg.springfx.bills.services;

import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import wg.springfx.bills.models.Income;
import wg.springfx.bills.repositories.IncomeRepository;

@Service
public class IncomeService {

    private IncomeRepository incomeRepository;

    @Autowired
    public IncomeService(IncomeRepository incomeRepository) {
        this.incomeRepository = incomeRepository;
    }

    public void addIncome(ChoiceBox<String> incomeCategory, TextField incomeValue) {
        String category = incomeCategory.getValue();
        Double value = null;
        try {
            value = Double.parseDouble(incomeValue.getText().replace(",", "."));
            incomeValue.clear();
            incomeValue.getStylesheets().clear();
            incomeValue.setPromptText("0.0");
        } catch (NumberFormatException e) {
            incomeValue.clear();
            incomeValue.setPromptText("Wrong Format!");
            incomeValue.getStylesheets().add("css/validation.css");
        }
        Income income = Income.builder().category(category).value(value).build();
        incomeRepository.save(income);
    }
}
